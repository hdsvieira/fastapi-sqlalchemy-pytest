from fastapi import FastAPI

from connections.database_sqlite import engine, Base

from api import api_router

Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(api_router)
