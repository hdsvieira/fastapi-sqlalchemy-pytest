import pytest
from schemas.users import ListUserResponse, UserResponse
from tests.conftest import client
from fastapi import status

def test_create_user():
    response = client.post(
        "/api/usuario",
        json={"name": "Usuario Teste"},
    )
    data = response.json()
    assert response.status_code == 201, response.text
    assert UserResponse.model_validate(data)

def test_list_all_user():
    response = client.get(
        "/api/usuario",
    )
    data = response.json()
    assert response.status_code == 200, response.text
    assert ListUserResponse.model_validate(data)

@pytest.mark.parametrize("test_input,expected", [(1, status.HTTP_200_OK), (2, status.HTTP_404_NOT_FOUND)])
def test_get_one_user(test_input, expected):
    response = client.get(
        f"/api/usuario/{test_input}",
    )
    data = response.json()
    assert response.status_code == expected, response.text
    if expected == status.HTTP_404_NOT_FOUND:
        assert data == {'detail': 'Curso não encontrado'}
        return
    assert UserResponse.model_validate(data)

@pytest.mark.parametrize("test_input,expected", [(1, status.HTTP_200_OK), (2, status.HTTP_404_NOT_FOUND)])
def test_put_one_user(test_input, expected):
    response = client.put(
        f"/api/usuario/{test_input}",
        json={"name": "Usuario Teste Atualizado"},
    )
    data = response.json()
    if expected == status.HTTP_404_NOT_FOUND:
        assert data == {'detail': 'Curso não encontrado'}
        return
    assert response.status_code == status.HTTP_200_OK, response.text
    assert UserResponse.model_validate(data)

@pytest.mark.parametrize("test_input,expected", [(1, status.HTTP_204_NO_CONTENT), (2, status.HTTP_404_NOT_FOUND)])
def test_delete_one_user(test_input, expected):
    response = client.delete(
        f"/api/usuario/{test_input}",
    )
    if expected == status.HTTP_404_NOT_FOUND:
        assert response.json() == {'detail': 'Curso não encontrado'}
        return
    assert response.status_code == status.HTTP_204_NO_CONTENT