# Projeto de um CRUD em FastAPI usando SqlAlchemy

Necessário Criar e Ativar um ambiente virtual:
- Criar:
```bash
python3.11 -m venv venv
```
- Ativar:
```bash
source venv/bin/activate
```

Necessário Instalação das bibliotecas: 
```bash
pip install -r requirements.txt
```

Executar o Projeto
```bash
uvicorn main:app --reload
```

Executar os Testes e Gerar o Report
```bash
pytest --cov-report html:htmlcov --cov=. -v
```