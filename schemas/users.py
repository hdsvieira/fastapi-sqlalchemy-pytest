from typing import List
from pydantic import BaseModel, ConfigDict

class UserBase(BaseModel):
    name: str

class UserRequest(UserBase):
    ...

class UserResponse(UserBase):
    model_config = ConfigDict(from_attributes=True)
    id: int

class ListUserResponse(BaseModel):
    results: List[UserResponse]
    count: int
