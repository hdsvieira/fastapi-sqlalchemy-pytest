from sqlalchemy import Column, Integer, String

from connections.database_sqlite import Base

class Users(Base):
    __tablename__ = "users"

    id: int = Column(Integer, primary_key=True, index=True)
    name: str = Column(String(100), nullable=False)
